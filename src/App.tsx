import React from "react";
import "./App.css";

function App() {
  return (
    <div className='App'>
      <h1>Leat's learn TypeScript with React</h1>
    </div>
  );
}

export default App;
