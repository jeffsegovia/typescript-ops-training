# Tickets Manager App

### A simple app for learning TypeScript & ReactJS

## To get started using this project

Clone the repository then do:

```
git pull origin starter
```

In the project directory, install the project's dependencies by running:

```
yarn install
```

Start the application by running:

```
yarn start
```
